const spiralize = function(size) {
  let res = Array(size)
    .fill(0)
    .map((n, i) => {
      if (i === 0 || i === size - 1) return Array(size).fill(1);
      else
        return Array(size)
          .fill(0)
          .map((x, j) => {
            if (j === 0 || j === size - 1) return 1;
            else return 0;
          });
    });
  res[1][0] = 0;
  let posX = 0;
  let posY = 2;
  let direction = "right";
  let running = true;
  let notSet = 0;

  while (running) {
    console.log(posY, posX);
    switch (direction) {
      case "right":
        posX++;
        if (res[posY][posX + 1] === 1) {
          direction = "down";
          posX--;
        }
        if (
          res[posY - 1][posX] !== 1 &&
          res[posY + 1][posX] !== 1 &&
          res[posY][posX + 1] !== 1
        ) {
          res[posY][posX] = 1;
          notSet = 0;
        } else {
          notSet++;
        }
        break;
      case "left":
        posX--;
        if (res[posY][posX - 1] === 1) {
          posX++;
          direction = "up";
        }
        if (
          res[posY - 1][posX] !== 1 &&
          res[posY + 1][posX] !== 1 &&
          res[posY][posX - 1] !== 1
        ) {
          res[posY][posX] = 1;
          notSet = 0;
        } else {
          notSet++;
        }
        break;
      case "down":
        posY++;
        if (res[posY + 1][posX] === 1) {
          direction = "left";
          posY--;
        }
        if (
          res[posY + 1][posX] !== 1 &&
          res[posY][posX + 1] !== 1 &&
          res[posY][posX - 1] !== 1
        ) {
          res[posY][posX] = 1;
          notSet = 0;
        } else {
          notSet++;
        }
        break;
      case "up":
        posY--;
        if (res[posY - 1][posX] === 1) {
          direction = "right";
          posY++;
        }
        if (
          res[posY - 1][posX] !== 1 &&
          res[posY][posX + 1] !== 1 &&
          res[posY][posX - 1] !== 1
        ) {
          res[posY][posX] = 1;
          notSet = 0;
        } else {
          notSet++;
        }
    }

    if (notSet > 1) {
      running = false;
    }
  }
  return res;
};

console.log(spiralize(5), [
  [1, 1, 1, 1, 1],
  [0, 0, 0, 0, 1],
  [1, 1, 1, 0, 1],
  [1, 0, 0, 0, 1],
  [1, 1, 1, 1, 1]
]);

console.log(spiralize(8), [
  [1, 1, 1, 1, 1, 1, 1, 1],
  [0, 0, 0, 0, 0, 0, 0, 1],
  [1, 1, 1, 1, 1, 1, 0, 1],
  [1, 0, 0, 0, 0, 1, 0, 1],
  [1, 0, 1, 0, 0, 1, 0, 1],
  [1, 0, 1, 1, 1, 1, 0, 1],
  [1, 0, 0, 0, 0, 0, 0, 1],
  [1, 1, 1, 1, 1, 1, 1, 1]
]);
