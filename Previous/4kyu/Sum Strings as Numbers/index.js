function sumStrings(a,b) {
  let res = '';
  let maxLength = a.length > b.length ? a.length : b.length;
  a = '0'.repeat(maxLength - a.length) + a
  b = '0'.repeat(maxLength - b.length) + b
  let head = 0;
  for (let i = maxLength - 1; i >= 0; i--) {
    let num = parseInt(a[i]) + parseInt(b[i]) + head;

    head = String(num).length > 1 ? 1 : 0;
    if (head === 1 && i !== 0) num = num - 10;
    if (num === 0 && i === 0) num = '';
    res = num + res
  }
  return res;
}


console.log(sumStrings('123','456'),'579')
console.log(sumStrings('00103', '08567'),'8670')
console.log(sumStrings('','5'),'5')
console.log(sumStrings('712569312664357328695151392', '8100824045303269669937'),'712577413488402631964821329')
