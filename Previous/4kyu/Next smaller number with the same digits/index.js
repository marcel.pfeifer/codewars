function nextSmaller(num) {
	let arr = String(num).split('');
	if (arr.length <= 1)
		return -1;
	for (let i = arr.length - 1; i > 0; i--) {
		if (arr[i] < arr[i - 1]) {
			let larger = nextLargerst(+arr[i - 1], i - 1, arr.slice(i));
			if (larger !== -1) {
				let res = +(arr.slice(0, i - 1).join('') + larger);
				if (String(res).split('').length === arr.length) return res
				return -1
			} else {
				return -1
			}
		}
	}
	return -1;
}

function nextLargerst(num, index, arr) {
	arr.sort(function (a, b) {
		return b - a;
	});
	for (let j = 0; j < arr.length; j++) {
		if (num > arr[j]) {
			if (arr[j] === 0 && index === 0)
				return -1;
			else {
				let temp1 = arr[j];
				arr[j] = num;
				num = temp1;
				break;
			}
		}
	}
	return [num].concat(arr).join('');
}

console.log(nextSmaller(21581957621) === 21581961257);
console.log(nextSmaller(21) === 12);
console.log(nextSmaller(907) === 790);
console.log(nextSmaller(531) === 513);
console.log(nextSmaller(135) === -1);
console.log(nextSmaller(2071) === 2017);
console.log(nextSmaller(1027) === -1);
console.log(nextSmaller(414) === 144);
