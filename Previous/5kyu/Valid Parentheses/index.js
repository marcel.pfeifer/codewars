function validParentheses(parens){
	let stack = [];
	let err = false
	parens.split('').forEach(p => {
		if (p === '(') {
			stack.push(p)
		} else {
			let r = stack.pop()
			if (!r) err = true
		}
	});
	if (err) return false
	return !stack.length;
}

console.log(validParentheses( "()" ), true);
console.log(validParentheses( "())" ), false);
