function longestConsec(strarr, k) {
    let arr = [];
    strarr.forEach((val, i) => {
        let str = val;
        let push = true;
        for (let n = 1; n < k; n++) {
            let part = strarr[i + n]
            if (part) {
             str += part
            } else {
              push = false;
            }
        }
        if (push) arr.push(str)
    })
    if (arr.length && k > 0) {
      return arr.reduce(function (a, b) { return a.length < b.length ? b : a; });
    }
    return '';
}

console.log(longestConsec(["zone", "abigail", "theta", "form", "libe", "zas"], 2), "abigailtheta")