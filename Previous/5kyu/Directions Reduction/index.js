function dirReduc(arr){
    let op = {
      NORTH: "SOUTH",
      SOUTH: "NORTH",
      EAST: "WEST",
      WEST: "EAST"
    }
    let res = arr.reduce((accumulator, currentValue) => {
      if (!accumulator.arr) accumulator.arr = []
      op[accumulator.last] === currentValue ? accumulator["arr"].pop() : accumulator["arr"].push(currentValue)
      accumulator.last = accumulator.arr[accumulator.arr.length - 1];
      return accumulator
    }, {})
    return res.arr
  }

  console.log(dirReduc(["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]), ["WEST"])