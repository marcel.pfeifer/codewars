function orderWeight(strng) {
  let arr = strng.split(" ")
  arr.sort((a, b) => {
    let q1 = getQS(a)
    let q2 = getQS(b)
    let res = q1 - q2
    if (res === 0) {
      return a.localeCompare(b)
    } else {
      return res  
    }
  })
  return arr.join(' ')
}

function getQS(num){
return num.toString().split('').reduce((acc, el) => {
  return acc ? parseInt(acc) + parseInt(el) : acc++ 
});
}

console.log(orderWeight("103 123 4444 99 2000"), "2000 103 123 4444 99")